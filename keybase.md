### Keybase proof

I hereby claim:

  * I am aexvir on github.
  * I am aexvir (https://keybase.io/aexvir) on keybase.
  * I have a public key ASDQ6WVZ66fjR0C_0_8QWEYP6YsgDRb8PGqT6HzDrc_Fhwo

To claim this, I am signing this object:

```json
{
  "body": {
    "key": {
      "eldest_kid": "0120d0e96559eba7e34740bfd3ff1058460fe98b200d16fc3c6a93e87cc3adcfc5870a",
      "host": "keybase.io",
      "kid": "0120d0e96559eba7e34740bfd3ff1058460fe98b200d16fc3c6a93e87cc3adcfc5870a",
      "uid": "fad8c1b31b0135d6c6bd59f4a4932d19",
      "username": "aexvir"
    },
    "merkle_root": {
      "ctime": 1527071080,
      "hash": "5934668275e2a71c877ee79fee17e51af1eae645d6bdf66ffe964d246b66b21924c19178c0de10c90011718f52b0a0945b35dd1be526a60cc64fb02bb3a227fa",
      "hash_meta": "8bfcb23cba7070bcd23d1c502bbad615f4ab193cb60bbb5ffa2c7769e2b73940",
      "seqno": 2879656
    },
    "service": {
      "entropy": "2Qqi6PAwpDIAfNeJoHVmSY9H",
      "name": "github",
      "username": "aexvir"
    },
    "type": "web_service_binding",
    "version": 2
  },
  "client": {
    "name": "keybase.io go client",
    "version": "1.0.48"
  },
  "ctime": 1527071084,
  "expire_in": 504576000,
  "prev": "eed9b79bddda94d3b08ff405887084faf2401a1fb6e66fd0c0913803f3f8c4d5",
  "seqno": 5,
  "tag": "signature"
}
```

with the key [ASDQ6WVZ66fjR0C_0_8QWEYP6YsgDRb8PGqT6HzDrc_Fhwo](https://keybase.io/aexvir), yielding the signature:

```
hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEg0OllWeun40dAv9P/EFhGD+mLIA0W/Dxqk+h8w63PxYcKp3BheWxvYWTESpcCBcQg7tm3m93alNOwj/QFiHCE+vJAGh+25m/QwJE4A/P4xNXEIBBsY24BDfhkYlESepVAiuvJCK5fXC4MIOX089I26Yp/AgHCo3NpZ8RA698/GA6+fw1ReuVJSOa96z2vVlxd+z+pgmU2RMCKDBKgOd3yeUx9lY6EY5xUlwsjGmhsx2x8zZ6Q06vHfFrQCahzaWdfdHlwZSCkaGFzaIKkdHlwZQildmFsdWXEINhc5S5cya+8i4guQGWGWKt60h4LMQQ/KNtGV595hYXwo3RhZ80CAqd2ZXJzaW9uAQ==

```

And finally, I am proving ownership of the github account by posting this as a gist.

### My publicly-auditable identity:

https://keybase.io/aexvir

### From the command line:

Consider the [keybase command line program](https://keybase.io/download).

```bash
# look me up
keybase id aexvir
```